package ru.pavlovka.homework5.files;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.*;
import java.util.stream.Collector;

public interface FileInfo {
    static List<String> listFileNames(File file, FileFilter fileFilter){
        List<String> list = new ArrayList<String>();
        if(file.isDirectory()){
            for(File subFile: file.listFiles(fileFilter)){
                list.add(subFile.getName());
            }
        }
        return list;
    }

    static List<String> listFileNames(String path, FileFilter fileFilter){
        return listFileNames(new File(path), fileFilter);
    }

    static List<String> listFileNames(String path, FilenameFilter filter){
        return listFileNames(new File(path), filter);
    }

    static List<String> listFileNames(File file, FilenameFilter filter){
        List<String> list = Arrays.asList(file.list(filter));
        return list;
    }

    static String extension(File file){
        String fileName=file.getName();
        return extension(fileName);
    }

    static String extension(String fileName){
        String extension = "";
        int index = fileName.lastIndexOf(".");
        if(index>=0){
            extension = fileName.substring(index);
        }
        return extension;
    }
}
