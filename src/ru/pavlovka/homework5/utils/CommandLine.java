package ru.pavlovka.homework5.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

public interface CommandLine {
    static String nextLine(String question){
        System.out.println(question);
        return new Scanner(System.in).nextLine();
    }

    static void printReflect(String string) {
        try {
            Method method=System.out.getClass().getMethod("println", String.class);
            method.invoke(System.out, string);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
