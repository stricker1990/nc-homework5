package ru.pavlovka.homework5.utils;

import java.lang.reflect.*;
import java.util.*;
import java.util.function.DoubleFunction;

public interface Metadata {

    static String getSuperClassname(Class classInfo){
        Class<?> superClass=classInfo.getSuperclass();
        if(superClass==null){
            return null;
        }
        return classInfo.getSuperclass().getCanonicalName();
    }

    static void printSuperclassName(Class<?> classInfo){
        System.out.println("Super class of "+classInfo.getCanonicalName()+" is "+getSuperClassname(classInfo));
    }

    static void printTypeInfo(Class<?> c){
        System.out.println(c.getCanonicalName());
        System.out.println(c.getName());
        System.out.println(c.toString());
        System.out.println(c.toGenericString());
        System.out.println(c.getSimpleName());
        System.out.println(c.getTypeName());
    }

    static List<Field> getAllFields(Class<?> c){
        List<Field> fields = new ArrayList<Field>(Arrays.asList(c.getDeclaredFields()));
        Class<?> currentClass=c.getSuperclass();
        while(true){
            if(currentClass==Object.class){
                break;
            }
            fields.addAll(Arrays.asList(currentClass.getDeclaredFields()));
            currentClass=currentClass.getSuperclass();
        }
        return fields;
    }

    static String universalToString(Object object) {

        if(object.getClass().isPrimitive()){
            return object.toString();
        }

        Class<?> c = object.getClass();

        String result = c.getSimpleName()+"{";

        List<String> fieldStrings = new LinkedList<>();

        for(Field field: getAllFields(c)){
            if(!field.isAccessible()){
                field.setAccessible(true);
            }
            try {

                if(field.getType().isPrimitive()){
                    fieldStrings.add(field.getName()+ " = " + field.get(object));
                }else{
                    fieldStrings.add(field.getName()+ " = " + universalToString(field.get(object)));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        result = result + String.join(", ", fieldStrings);

        result=result+"}";
        return result;
    }

    static void printAllMethods(Class<?> cl){
        while (cl != null) {
            for (Method m : cl.getDeclaredMethods()) {
                System.out.println(
                        Modifier.toString(m.getModifiers()) + " " +
                                m.getReturnType().getCanonicalName() + " " +
                                m.getName() +
                                Arrays.toString(m.getParameters()));
            }
            cl = cl.getSuperclass();
        }
    }

    static Method findMethod(Class<?> cl, String methodName) {
        Method m=null;
        while (cl!=null){
            try {
                m = cl.getMethod(methodName, Double.TYPE);
                break;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            cl=cl.getSuperclass();
        }
        return m;
    }

    static void printDoubleMethods(Class<?> cl, String methodName, double start, double end, double step){
        Method m = findMethod(cl, methodName);
        if(m==null){
            return;
        }
        for(double current=start; current<=end; current+=step){
            try {
                System.out.println(m.getName()+"("+current+")="+m.invoke(null, current));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    static void printDoubleMethods(String functionName, DoubleFunction<Object> f, double start, double end, double step){
        for(double current=start; current<=end; current+=step){
            System.out.println(functionName+"("+current+")="+f.apply(start));
        }
    }
}
