package ru.pavlovka.homework5.sort;

import java.io.File;
import java.util.*;

public interface ArraySort {
    static void luckySort(ArrayList<String> list, Comparator<String> comparator){
        while (true){
            boolean sorted = true;
            for(int i=0; i<list.size()-1; i++){
                if(comparator.compare(list.get(i), list.get(i+1))>=1){
                    sorted = false;
                }
            }
            if(sorted){
                return;
            }
            Collections.shuffle(list);
        }
    }

    static void sortFilesByDirectoriesAndFilenames(File[] files){
        Arrays.sort(files, (File file1, File file2)->{
            if(file1.isDirectory() && !file2.isDirectory()){
                return -1;
            }
            if(!file1.isDirectory() && file2.isDirectory()){
                return 1;
            }
            return file1.getName().compareTo(file2.getName());
        });
    }
}
