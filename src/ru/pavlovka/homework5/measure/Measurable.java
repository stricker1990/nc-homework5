package ru.pavlovka.homework5.measure;

public interface Measurable {
    double getMeasure();

    public static double average(Measurable[] measurableArray){
        double sum = 0;
        for(Measurable measurable: measurableArray){
            sum += measurable.getMeasure();
        }
        return sum/measurableArray.length;
    }

    public static Measurable largest(Measurable[] measurableArray){
        Measurable measurableLargest =null;
        double max = 0;
        for(Measurable measurable: measurableArray){
            double measure = measurable.getMeasure();
            if(Double.compare(max, measure)==1){
                max = measure;
            }
            measurableLargest = measurable;
        }
        return measurableLargest;
    }
}
