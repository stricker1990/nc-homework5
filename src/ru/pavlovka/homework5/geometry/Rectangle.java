package ru.pavlovka.homework5.geometry;

import java.util.Objects;

public class Rectangle extends Shape implements Cloneable{

    private double width;
    private double height;

    public Rectangle(Point topLeft, double width, double height){
        super(topLeft);
        this.width = width;
        this.height = height;
    }

    @Override
    public Point getCenter() {
        return new Point(getPoint().getX()+width/2, getPoint().getY()+height/2);
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Point getTopLeft(){
        return getPoint();
    }

    public void setTopLeft(Point point){
        setPoint(point);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.width, width) == 0 &&
                Double.compare(rectangle.height, height) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), width, height);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "topLeft=" + getTopLeft() +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Rectangle((Point) getPoint().clone(), width, height);
    }
}
