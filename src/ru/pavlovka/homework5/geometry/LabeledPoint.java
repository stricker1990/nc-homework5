package ru.pavlovka.homework5.geometry;

import java.util.Objects;

public class LabeledPoint extends Point implements Cloneable {

    private String label;

    public LabeledPoint(String label, double x, double y) {
        super(x, y);
        this.label = label;
    }

    public String getLabel(){return label;}

    @Override
    public String toString() {
        return "LabeledPoint{" +
                "label='" + label + '\'' +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LabeledPoint that = (LabeledPoint) o;
        return Objects.equals(label, that.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), label);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
