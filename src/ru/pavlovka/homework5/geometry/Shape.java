package ru.pavlovka.homework5.geometry;

import ru.pavlovka.homework5.utils.Metadata;

import java.util.Objects;

public abstract class Shape implements Cloneable {
    private Point point;

    public Shape(Point point){
        this.point = point;
    }

    protected Point getPoint(){ return point; }

    protected void setPoint(Point point){ this.point = point; }

    public void moveBy(double dx, double dy){
        this.point.setX(point.getX()+dx);
        this.point.setY(point.getY()+dy);
    }

    public abstract Point getCenter();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shape shape = (Shape) o;
        return Objects.equals(point, shape.point);
    }

    @Override
    public int hashCode() {
        return Objects.hash(point);
    }

    @Override
    public String toString() {
        return Metadata.universalToString(this);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
