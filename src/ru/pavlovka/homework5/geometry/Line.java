package ru.pavlovka.homework5.geometry;

import java.util.Objects;

public class Line extends Shape implements Cloneable {

    private Point to;

    public Line(Point from, Point to){
        super(from);
        this.to=to;
    }

    public Point getTo(){
        return to;
    }

    public void setTo(Point to){
        this.to=to;
    }

    public Point getFrom(){
        return getPoint();
    }

    public void setFrom(Point from){
        setPoint(from);
    }

    @Override
    public Point getCenter() {
        double dx = (getTo().getX() - getFrom().getX())/2;
        double dy = (getTo().getY() - getFrom().getY())/2;
        return new Point(dx, dy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Line line = (Line) o;
        return Objects.equals(to, line.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), to);
    }

    @Override
    public String toString() {
        return "Line{" +
                "from=" + getFrom() +
                ", to=" + to +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Line((Point)getFrom().clone(), (Point) to.clone());
    }
}
