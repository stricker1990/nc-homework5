package ru.pavlovka.homework5.geometry;

import ru.pavlovka.homework5.utils.Metadata;

import java.util.Objects;

public class Circle extends Shape implements Cloneable{

    private double radius;

    public Circle(Point center, double radius){
        super(center);
        this.radius = radius;

    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public Point getCenter() {
        return getPoint();
    }

    @Override
    public String toString() {
        return Metadata.universalToString(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), radius);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Circle((Point) getPoint().clone(), radius);
    }
}
