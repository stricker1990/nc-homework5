package ru.pavlovka.homework5.sequences;

public interface IntSequence {
    static IntSequence of(int... sequence){
        return new IntSequence() {
            private int index = 0;

            @Override
            public int next() {
                if(sequence.length==0){
                    return 0;
                }
                int result = sequence[index];
                if(index<sequence.length-1){
                    index++;
                }else{
                    index=0;
                }
                return result;
            }

            @Override
            public boolean hasNext(){
                return index<sequence.length-1;
            }
        };
    }

    static IntSequence constant(int value){
        return ()->value;
    }

    default boolean hasNext(){return true;}

    int next();
}
