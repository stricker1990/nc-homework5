package ru.pavlovka.homework5.sequences;

public interface Sequence<T> {
    default boolean hasNext(){return true;}
    T next();
}
