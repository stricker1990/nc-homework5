package ru.pavlovka.homework5.sequences;

import java.math.BigInteger;

public class SquareSequences implements Sequence<BigInteger> {

    private BigInteger number;

    public SquareSequences(int number){
        this(BigInteger.valueOf(number));
    }

    public SquareSequences(BigInteger number){
        this.number = number;
    }

    public BigInteger getNumber() {
        return number;
    }

    public void setNumber(BigInteger number) {
        this.number = number;
    }

    @Override
    public BigInteger next() {

        number = number.multiply(number);
        return number;
    }
}
