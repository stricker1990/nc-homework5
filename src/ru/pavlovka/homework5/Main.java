package ru.pavlovka.homework5;

import ru.pavlovka.homework5.colors.Color;
import ru.pavlovka.homework5.emloyeers.Employee;
import ru.pavlovka.homework5.files.FileInfo;
import ru.pavlovka.homework5.geometry.*;
import ru.pavlovka.homework5.items.DiscountedItem;
import ru.pavlovka.homework5.items.Item;
import ru.pavlovka.homework5.measure.Measurable;
import ru.pavlovka.homework5.multithread.Greeter;
import ru.pavlovka.homework5.multithread.TaskRunner;
import ru.pavlovka.homework5.sequences.IntSequence;
import ru.pavlovka.homework5.sequences.SquareSequences;
import ru.pavlovka.homework5.sort.ArraySort;
import ru.pavlovka.homework5.utils.CommandLine;
import ru.pavlovka.homework5.utils.Metadata;

import javax.imageio.stream.ImageOutputStream;
import java.io.File;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        testFiles();
        testEmpoyeers();
        testSuperClassNames();
        testSequence();
        testArraySort();
        testGeometry();
        testItems();
        testColors();
        testTypeInfo();
        testUniversalToString();
        testMethodPrinter();
        testPlrintLn();
        testDoubleMethods();

        TaskRunner.runInOrder(new Greeter(2, "1"), new Greeter(2, "2"));
        TaskRunner.runTogether(new Greeter(2, "1"), new Greeter(2, "2"));

    }

    private static void testEmpoyeers(){
        Measurable[] measurableArray={new Employee("Иван", "Иванов", 100), new Employee("Петр", "Петров", 200)};
        System.out.println("average: "+Measurable.average(measurableArray));
        System.out.println("largest: "+((Employee)Measurable.largest(measurableArray)).fullName());
    }

    public static void testSuperClassNames(){
        Metadata.printSuperclassName(String.class);
        Metadata.printSuperclassName(Scanner.class);
        Metadata.printSuperclassName(ImageOutputStream.class);
    }

    public static void testSequence(){
        IntSequence sequence = IntSequence.of(3, 1, 4, 1, 5, 9);
        SquareSequences squareSequences = new SquareSequences(2);

        while (sequence.hasNext()){
            System.out.println(sequence.next());
        }

        for(int i=0; i< 2; i++){
            System.out.println("Infinity: "+ IntSequence.constant(1).next());
            System.out.println("Square: "+ squareSequences.next());
        }
    }

    public static void testArraySort(){
        ArrayList<String> list = new ArrayList<>();
        list.add("C");
        list.add("B");
        list.add("A");
        ArraySort.luckySort(list, (String s1, String s2)-> s1.compareTo(s2));
        System.out.println(list);
    }

    public static void testFiles(){
        String path = System.getProperty("user.dir");
        System.out.println("Subdirs:");
        for(String filepath: FileInfo.listFileNames(path, (File subFile)-> subFile.isDirectory())){
            System.out.println(filepath);
        }
        System.out.println("Extensions:");
        for(String filepath: FileInfo.listFileNames(path, (File file, String name)->".md".equals(FileInfo.extension(name)))){
            System.out.println(filepath);
        }
        System.out.println("Sorted:");
        File[] allFiles = new File(path).listFiles();
        ArraySort.sortFilesByDirectoriesAndFilenames(allFiles);
        for(File file: allFiles){
            System.out.println(file.getName());
        }
    }

    public static void testGeometry(){
        System.out.println(new LabeledPoint("Test", 0, 0));
        System.out.println(new Circle(new Point(0, 0), 2).getCenter());
        System.out.println(new Rectangle(new Point(0, 0), 10, 20).getCenter());
        System.out.println(new Line(new Point(0, 0), new Point(10, 20)).getCenter());

        try {
            System.out.println(new LabeledPoint("Test", 1, 1).clone());
            System.out.println(new Circle(new Point(1, 1), 2).clone());
            System.out.println(new Rectangle(new Point(1, 1), 10, 20).clone());
            System.out.println(new Line(new Point(1, 1), new Point(10, 20)).clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    public static void testItems(){
        Item item1 = new Item("test", 100);
        Item item2 = new DiscountedItem("test", 100, 0);
        Item item3 = new DiscountedItem("test", 150, 50);

        System.out.println(""+item1+" "+item2+" = "+item1.equals(item2));
        System.out.println(""+item2+" "+item3+" = "+item2.equals(item3));
        System.out.println(""+item1+" "+item3+" = "+item1.equals(item3));
    }

    public static void testColors(){
        for(Color color: Color.values()){
            System.out.println(""+color.getRed()+" "+color.getGreen()+" "+color.getBlue());
        }
    }

    public static void testTypeInfo(){
        System.out.println("Generic type: ");
        Metadata.printTypeInfo(new ArrayList<Object>().getClass());
        System.out.println("Simple Object Type: ");
        Metadata.printTypeInfo(Circle.class);
        System.out.println("Array type: ");
        Metadata.printTypeInfo(new int[10].getClass());
        System.out.println("Integer type: ");
        Metadata.printTypeInfo(Integer.valueOf(1).getClass());
        System.out.println("Anonymous type: ");
        Metadata.printTypeInfo(IntSequence.of(1, 2, 3).getClass());
    }

    public static void testUniversalToString(){
        System.out.println("Universal to string:");
        System.out.println(new Circle(new Point(1, 2), 20));
    }

    public static void testMethodPrinter(){
        Metadata.printAllMethods(int[].class);
    }

    public static void testPlrintLn(){
        long start = System.nanoTime();
        CommandLine.printReflect("Hello world!");
        long end = System.nanoTime();

        System.out.println("reflection println perfom: "+(end-start));

        start = System.nanoTime();
        System.out.println("Hello world!");
        end = System.nanoTime();

        System.out.println("regular println: "+(end-start));
    }

    public static void testDoubleMethods(){
        System.out.println("with reflection:");
        Metadata.printDoubleMethods(Math.class, "sqrt", 0, 100, 10);
        Metadata.printDoubleMethods(Double.class, "toHexString", 0, 100, 10);

        System.out.println("with functional interface:");
        Metadata.printDoubleMethods("sqrt",Math::sqrt, 0, 100, 10);
        Metadata.printDoubleMethods("toHexString", Double::toHexString, 0, 100, 10);
    }

}
