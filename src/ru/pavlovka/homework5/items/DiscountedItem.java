package ru.pavlovka.homework5.items;

import java.util.Objects;

public class DiscountedItem extends Item {
    private double discount;

    public DiscountedItem(String description, double price, double discount) {
        super(description, price);
        this.discount = discount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object o) {
        if(o.getClass()==Item.class){
            if (!super.equals(o)) return false;
        }
        DiscountedItem other = (DiscountedItem) o;
        return Double.compare(getPrice()-discount, other.getPrice()-other.discount)==0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), discount);
    }

    @Override
    public String toString() {
        return "DiscountedItem{" +
                "discount=" + discount +
                '}';
    }
}
