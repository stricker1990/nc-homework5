package ru.pavlovka.homework5.multithread;

public interface TaskRunner {
    static void runTogether(Runnable ...tasks){
        for(Runnable task: tasks){
            new Thread(task).start();
        }
    }

    static void runInOrder(Runnable ...tasks){
        for(Runnable task: tasks){
            task.run();
        }
    }
}
